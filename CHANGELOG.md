# Changelog

## 3.3.1 - 2025-12-21

### Added

- require `spip/archiviste=^3.0`

## 3.3.0 - 2025-11-25

### Added

- Installable en tant que package Composer

### Changed

- spip/spip#6043 l'attribut "module" sur balise script diffère l'exécution et évite les erreurs avec le nouveau formalisme de `ajaxCallBack.js`
- spip/spip#5460 `style_prive_plugin_mots` sans compilation de SPIP
- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev

### Fixed

- #4913 Tenir compte de l'autorisation de configuration d'un plugin pour afficher le bouton Configurer
